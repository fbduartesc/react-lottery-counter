import './App.css'
import React from 'react'

import Mega from "./components/mega/Mega"
import Counter from './components/counter/Counter'
import Card from './components/layout/Card'

export default () => (
    <div className="App">
        <h1>React fundamentals</h1>

        <div className="Cards">
            <Card titulo="Mega-sena lottery" color="#2B9464">
                <Mega quantity={6}></Mega>
            </Card>

            <Card titulo="Counter" color="#DE5842">
                <Counter numberInitial={10}></Counter>
            </Card>           
        </div>
    </div>
)