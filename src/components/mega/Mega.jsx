import "./Mega.css"
import React, { useState } from "react"

export default (props) => {
    function generateNumberNotContained(min, max, array) {
        const random = parseInt(Math.random() * (max + 1 - min)) + min
        return array.includes(random)
            ? generateNumberNotContained(min, max, array)
            : random
    }

    function generateNumbers(quantity) {
        const numbers = Array(quantity)
            .fill(0)
            .reduce((nums) => {
                const newNumber = generateNumberNotContained(1, 60, nums)
                return [...nums, newNumber]
            }, [])
            .sort((n1, n2) => n1 - n2)

        return numbers
    }

    const [quantity, setQuantity] = useState(props.quantity || 6)
    const numbersInitials = generateNumbers(quantity)
    const [numbers, setNumbers] = useState(numbersInitials)

    return (
        <div className="Mega">
            <h2>Mega</h2>
            <h3>{numbers.join(" ")}</h3>
            <div>
                <label>Numbers</label>
                <input
                    min="6"
                    max="15"
                    type="number"
                    value={quantity}
                    onChange={(e) => {
                        setQuantity(+e.target.value)
                        setNumbers(generateNumbers(+e.target.value))
                    }}
                />
            </div>
            <button onClick={(_) => setNumbers(generateNumbers(quantity))}>
                Generate
            </button>
        </div>
    )
}